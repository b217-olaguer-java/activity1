// A "package" in Java is used to group related classes. Think of it as a folder in a directory
    // Packages are divided into two categories
        // 1. Built-in Packages - (packages from JAVA API)
        // 2. User-defined Packages
// Package creation follow the "reverse domain name notation" for the naming convention

package com.zuitt.example;

public class Variables {
    public static void main(String[] args) {

        // Variables
        // let age = "This is a string";
        int age;
        char middleInitial;

        // Variable Declaration vs Initialization
        int x;
        int y = 0;

        // Initialization after declaration
        x = 1;

        //
        System.out.println("The value of y is " + y + " and the value of x is " + x);

        // Primitive Data types
        // Predefined within the Java Programming Language which is used for "single-valued" variables with limited capabilities

        // int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        // Long
        // "L" is added at the end of the long number to be recognized
        long worldPopulation = 6845783492314589034L;
        System.out.println(worldPopulation);

        // float
        // add f at the end of the float to be recognized
        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

        // double
        double piDouble = 3.14159265359;
        System.out.println(piDouble);

        // char - single character
        char letter = 'a';
        System.out.println(letter);

        // boolean
        boolean isLove = true;
        boolean isTaken = false;
        System.out.println(isLove);
        System.out.println(isTaken);

        isTaken = true;
        System.out.println(isTaken);

        // Constant
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        final int principal = 45000;
        System.out.println(principal);

        // Non-Primitive Data Type
            // also known as reference data types refering to instances or objects
        // String
        // Strings are actually objects that can use methods
        // Stores a sequence or array of characters

        String userName = "JSmith";
        System.out.println(userName);

        int stringLength = userName.length();
        System.out.println(stringLength);
    }



}
