package com.zuitt.example;
import java.util.Scanner;

public class Activity {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter First Name:");
        String firstName = myObj.nextLine();

        System.out.println("Enter Last Name:");
        String lastName = myObj.nextLine();

        System.out.println("What is your First Subject grade?");
        double firstSubjectGrade = myObj.nextDouble();

        System.out.println("What is your Second subject grade?");
        double secondSubjectGrade = myObj.nextDouble();

        System.out.println("What is your Third subject grade?");
        double thirdSubjectGrade = myObj.nextDouble();

        double gradeAverage = ((firstSubjectGrade + secondSubjectGrade + thirdSubjectGrade)/3);

        System.out.println("First Name:");
        System.out.println(firstName);

        System.out.println("Last Name:");
        System.out.println(lastName);

        System.out.println("First Subject Grade:");
        System.out.println(firstSubjectGrade);

        System.out.println("Second Subject Grade");
        System.out.println(secondSubjectGrade);

        System.out.println("Third Subject Grade");
        System.out.println(thirdSubjectGrade);

        System.out.println("Good day, " + firstName + " " + lastName + ".");
        System.out.println("Your grade average is: " + gradeAverage);
    }
}
