package com.zuitt.example;
import java.util.Scanner;

public class TypeConversion {
    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);
        System.out.println("Enter numbers to add.");
        System.out.println("Enter first number: ");

//        int num1 = Integer.parseInt(myObj.nextLine());
        double num1 = myObj.nextDouble();

        System.out.println("Enter second number: ");
        int num2 = myObj.nextInt();

        System.out.println("The quotient of the two numbers is " + (num1 / num2));
    }
}
